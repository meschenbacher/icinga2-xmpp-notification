# xmpp notifications for icinga2

Based on a [netways block post from
2014](https://www.netways.de/blog/2014/07/17/jabber-notifications-mit-icinga-2/) with an
updated xmpp library ([slixmpp](https://github.com/poezio/slixmpp), previously [sleekxmpp](https://github.com/fritzy/SleekXMPP/)).

## Problems with python-xmpp
- Only resolves DNS A records and aborts if a host is not associated with a A record.

## Problems with sleekxmpp
- Is now deprecated

# Dependencies

Package `python3-slixmpp`

# Installation

Copy `xmpp-notification.py` to a location of your choosing and remember the location. Make
sure it is executable by the icinga user (e.g. with `chmod +x`).

Fill in `templates.conf` (usually in `/etc/icincga2/conf.d` or
`/etc/icinga2/zones.d/global-templates`):

```
template NotificationCommand "jabber-template" {
	import "plugin-notification-command"
	command = [
		"/usr/local/bin/xmpp-notification.py", # fill in the path to the script
		"$xmpp_recipient$",
		"$xmpp_message$"
	]
	vars.xmpp_recipient = "$jabberid$"
	env = {
		XMPP_USER = "$xmpp_user$"
		XMPP_PASSWORD = "$xmpp_password$"
	}
}

template NotificationCommand "jabber-host-notification" {
	import "jabber-template"
	vars.xmpp_message = {{{$notification.type$ $host.display_name$ $host.state$
Address6: $address6$
Address: $address$
Date/Time: $icinga.long_date_time$
Additional Info: $host.output$
Comment: [$notification.author$] $notification.comment$}}}
}

template NotificationCommand "jabber-service-notification" {
	import "jabber-template"
	vars.xmpp_message = {{{$notification.type$ $host.display_name$/$service.name$ $service.state$
Address6: $address6$
Address: $address$
Date/Time: $icinga.long_date_time$
Additional Info: $service.output$
Comment: [$notification.author$] $notification.comment$}}}
}
```

Fill in `commands.conf`:
```
object NotificationCommand "jabber-host-icinga" {
	import "jabber-host-notification"
	vars.xmpp_user = "sender@example.org"
	vars.xmpp_password = "password"
}
object NotificationCommand "jabber-service-icinga" {
	import "jabber-service-notification"
	vars.xmpp_user = "sender@example.org"
	vars.xmpp_password = "password"
}
```

Fill in `notifications.conf`:
```
apply Notification "jabber-host" to Host {
        command = "jabber-host-icinga"
        interval = 120m
        users = [ "icingaadmin" ]
        assign where true
}

apply Notification "jabber-service" to Service {
        command = "jabber-service-icinga"
        interval = 120m
        users = [ "icingaadmin" ]
        assign where true
}
```

Fill in `users.conf`:
```
object User "icingaadmin" {
[...]
        vars.jabberid = "recipient@example.org"
[...]
}
```
